a=(1+2)**3
b="Da"*4

# indique une erreur c="Da"+3
d=("Pa"+"La")*2

# indique une autre erreur ("Da"*4)/2

e=5/2
f=5//2
g=5%2
print(f"{a} {b} {d} {e} {f} {g}")