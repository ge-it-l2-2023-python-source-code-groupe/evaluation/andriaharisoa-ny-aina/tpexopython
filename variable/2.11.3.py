# Instructions 1 et 2
print("str(4) * int('3'):", str(4) * int("3"))
print("int('3') + float('3.2'):", int("3") + float("3.2"))

# La chaîne de caractères "3" est multipliée par le nombre entier 2
print("str(3) * 2:", str(3) * 2)

# Quatrième instruction
print("str(3/4) * 2:", str(3/4) * 2)
