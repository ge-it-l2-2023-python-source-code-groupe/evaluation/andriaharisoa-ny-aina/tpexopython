import os
import sys

def main():
  # Obtenez la liste des dossiers dans le répertoire courant.
  directories = os.listdir()

  # Demandez à l'utilisateur de choisir un dossier.
  directory_name = input("Choisissez un dossier : ")

  # Obtenez la liste des fichiers dans le dossier sélectionné.
  files = os.listdir(directory_name)

  # Créez un menu.
  menu = ""
  for file in files:
    menu += f"{file}\n"

  # Demandez à l'utilisateur de choisir un fichier.
  file_name = input("Choisissez un fichier : ")

  # Lancez le fichier sélectionné.
  os.system(f"python {directory_name}/{file_name}")

     # Demandez à l'utilisateur s'il veut retourner au menu principal.
  choice = input("Retourner au menu principal (o/n) : ")

    # Si l'utilisateur a choisi "o", retournez au menu principal.
  if choice == "o":
    return

if __name__ == "__main__":
  main()
