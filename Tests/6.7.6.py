# Boucle sur les nombres de 0 à 20
for i in range(0, 21):

  # Si i est pair et inférieur ou égal à 10,
  # alors il est affiché
  if i % 2 == 0 and i <= 10:
    print("pair ",i)

  # Sinon, si i est impair et supérieur ou égal à 10,
  # alors il est affiché avec un astérisque
  elif i % 2 != 0 and i >= 10:
    print("impair ", i)
