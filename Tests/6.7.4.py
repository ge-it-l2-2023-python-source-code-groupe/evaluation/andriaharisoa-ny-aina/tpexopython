# Déclaration des variables
séquence = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]

# Initialisation des fréquences
fréquences = {
    "A": 0,
    "R": 0,
    "W": 0,
    "G": 0,
}

# Boucle sur la séquence
for acide_aminé in séquence:

  # Incrémente la fréquence de l'acide aminé
  fréquences[acide_aminé] += 1

# Affichage des fréquences
print(fréquences)
