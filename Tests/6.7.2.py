# Déclaration des variables
sequence = ["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"]

# Initialisation de la séquence complémentaire
sequence_complementaire = []

# Boucle sur la séquence d'ADN
for nucléotide in sequence:

  # Remplace le nucléotide par son complémentaire
  if nucléotide == "A":
    sequence_complementaire.append("T")
  elif nucléotide == "T":
    sequence_complementaire.append("A")
  elif nucléotide == "C":
    sequence_complementaire.append("G")
  else:
    sequence_complementaire.append("C")

# Affichage de la séquence complémentaire
print(sequence_complementaire)
