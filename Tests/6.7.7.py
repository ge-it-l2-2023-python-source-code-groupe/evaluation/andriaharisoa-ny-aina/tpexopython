n = int(input("Entrez un nombre positif : "))
suite = []
for i in range(n):
  if n % 2 == 0:
    n = n // 2
  else:
    n = 3 * n + 1
  suite.append(n)
print("Les premiers nombres de la suite de Syracuse à partir de", n, "sont :", suite)
