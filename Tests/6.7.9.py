print("**********methode 1*************")
# Initialisation de la liste des nombres premiers
nombres_premiers = []

# Parcours des nombres de 2 à 100
for i in range(2, 101):

    # On vérifie si le nombre est premier
    premier = True
    for j in range(2, i):
        if i % j == 0:
            premier = False
            break

    # Si le nombre est premier, on l'ajoute à la liste
    if premier:
        nombres_premiers.append(i)

# Affichage de la liste des nombres premiers
print(nombres_premiers)

print("**********methode 2*************")
# Initialisation de la liste des nombres premiers
nombres_premiers = [2]

# Parcours des nombres de 3 à 100
for i in range(3, 101):

    # On vérifie si le nombre est composé
    composé = False
    for p in nombres_premiers:
        if i % p == 0:
            composé = True
            break

    # Si le nombre n'est pas composé, on l'ajoute à la liste
    if not composé:
        nombres_premiers.append(i)

# Affichage de la liste des nombres premiers
print(nombres_premiers)
