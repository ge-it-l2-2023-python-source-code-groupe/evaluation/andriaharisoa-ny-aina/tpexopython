# Déclaration des variables
liste = [8, 4, 6, 1, 5]

# Initialisation du minimum
minimum = liste[0]

# Boucle sur la liste
for élément in liste:

  # Met à jour le minimum si nécessaire
  if élément < minimum:
    minimum = élément

# Affichage du minimum
print(minimum)
