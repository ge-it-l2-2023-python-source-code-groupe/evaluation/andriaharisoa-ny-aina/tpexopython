# Déclaration des variables
notes = [14, 9, 13, 15, 12]

# Calcul de la note maximum
note_max = max(notes)

# Calcul de la note minimum
note_min = min(notes)

# Calcul de la moyenne
moyenne = sum(notes) / len(notes)

# Affichage de la moyenne
print("Moyenne :", moyenne)

# Calcul de la mention
if moyenne >= 10 and moyenne < 12:
  mention = "passable"
elif moyenne >= 12 and moyenne < 14:
  mention = "assez bien"
else:
  mention = "bien"

# Affichage de la mention
print("Mention :", mention)
