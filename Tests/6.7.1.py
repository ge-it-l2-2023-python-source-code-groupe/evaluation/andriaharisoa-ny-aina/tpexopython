semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

for jour in semaine:
  if jour in ["lundi", "mardi", "mercredi", "jeudi"]:
    print(f"Au travail, c'est {jour}")
  elif jour == "vendredi":
    print(f"Chouette, c'est vendredi !")
  else:
    print(f"Repos ce week-end, c'est {jour}")
