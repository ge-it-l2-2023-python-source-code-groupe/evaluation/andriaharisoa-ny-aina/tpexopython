nombre_secret = int(input("Pensez à un nombre entre 1 et 100 : "))
a = 1
b = 100

while a < b:
    m = (a + b) // 2
    réponse = input(f"Est-ce que votre nombre est plus grand, plus petit ou égal à {m} ? [+/-/=]")

    if réponse == "=":
        print(f"J'ai trouvé en {a + b} questions !")
        break
    elif réponse == "-":
        b = m - 1
    else:
        a = m + 1

if a == b:
    print(f"J'ai trouvé en {a} questions !")
