angles = [[48.6, 53.4], [-124.9, 156.7], [-80.6, -43.1], [-73.9, -26.0], [-68.5, -45.5], [-69.6, -38.3], [-61.2, 156.7], [-66.2, -40.6], [-53.7, 135.0], [-64.9, -41.0], [-62.7, -49.1], [-59.7, -30.8]]

for angle in angles:
  phi = angle[0]
  psi = angle[1]

  # Testons si l'angle phi est compris entre -57 et -27 degrés

  phi_in_range = -57 <= phi <= -27

  # Testons si l'angle psi est compris entre -47 et -23 degrés

  psi_in_range = -47 <= psi <= -23

  # Si les deux angles sont compris dans la plage, alors l'acide aminé est en hélice

  is_helix = phi_in_range and psi_in_range

  # Affichons les résultats

  print(f"{angle} : {'est' if is_helix else 'n est pas'} en hélice")
