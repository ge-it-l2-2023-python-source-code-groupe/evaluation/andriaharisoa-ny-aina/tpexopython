import random

# Initialise la position de la puce
position = 0

# Simule les sauts de la puce
while position != 5:
  # Tire un nombre aléatoire entre -1 et 1
  direction = random.choice([-1, 1])
  position += direction
  print(f"emplacement {position}")

# Affiche le nombre de sauts
print(f"Nombre de sauts: {position}")
