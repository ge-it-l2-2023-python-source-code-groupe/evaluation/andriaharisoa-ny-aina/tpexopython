l = ["vache", "souris", "levure", "bacterie"]

# Méthode 1
for i in l:
  print(i)

print("**************")
# Méthode 2
for i in range(len(l)):
  print(l[i])

print("**************")
# Méthode 3
while len(l) > 0:
  print(l.pop())
