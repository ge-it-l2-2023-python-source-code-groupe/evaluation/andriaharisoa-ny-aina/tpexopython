# Initialise la suite de Fibonacci
fibo = [0, 1]

# Calcule les 15 premiers termes de la suite
for i in range(2, 16):
  fibo.append(fibo[i - 1] + fibo[i - 2])

# Affiche la suite
for i in range(len(fibo)):
  print(f"x{i} = {fibo[i]}")

# Calcule le rapport entre les deux termes consécutifs
for i in range(1, len(fibo)):
  print(f"x{i} / x{i - 1} = {fibo[i] / fibo[i - 1]:.2f}")
