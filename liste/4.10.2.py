hiver = ["décembre", "janvier", "février"]
printemps = ["mars", "avril", "mai"]
été = ["juin", "juillet", "août"]
automne = ["septembre", "octobre", "novembre"]

saisons = [hiver, printemps, été, automne]

# 1. saisons[2]
saisons[2]

# 2. saisons[1][0]
saisons[1][0]

# 3. saisons[1:2]
saisons[1:2]

# 4. saisons[:][1]
saisons[:][1]
